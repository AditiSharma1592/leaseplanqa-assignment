# Leaseplan QA Assignment

This is a basic Rest API test framework for testing the https://waarkoop-server.herokuapp.com endpoint for searching the product.Tests are written using a combination of SerenityBDD, Rest Assured, Cucumber, Junit & Maven.

## Framework Structure
•API calls & validations are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured.
•Tests are written in BDD Gherkin format in Cucumber feature files, and it is available within the resources/feature folder..
•Action package consists of action classes for the endpoint. It can be defined based on the endpoints .
•Action classes are called from a step-definitions class which are available within stepDefinitions package.
•The schema for comparing response is placed inside "src/test/resources/schema". 

### The project directory structure

    src/test/java
     + leaseplan.actions               Package for API actions
            SearchProductsActions      API calls and User actions of search product
       
    + leaseplan.stepdefinitions        Package for Step definitions for the BDD feature
            SearchStepDefinitions      Step Definition for searchProduct
            
    + leaseplan.stepdefinitions        Package for common steps like building request
            HTTPRequests               Building API requests
            
    + leaseplan.testrunner             
            TestRunner                 TestRunner file
            
    src/test/resources
      + features                       Folder for all feature files
          search_product.feature       Feature containing BDD scenarios for search product endpoint
      + schema                         Folder containing json schema for API schema validation
      Serenity.conf                    Configurations file
      

## How to run
1. Run mvn clean verify from the command line. OR
2. From test Runner file

## Reports
The test results will be recorded here target/site/serenity/index.html.Reports can be seen in gitlab-ci, on the artifacts section.

## Refactoring Done
1. Update POM.xml with latest version and removing unwanted dependencies
2. Update the folder structure to make the code more easy to maintain
3. Try to cover the maximum scenarios in single scenario outline using Examples for code resusablity

