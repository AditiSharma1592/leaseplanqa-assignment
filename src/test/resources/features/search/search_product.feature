Feature: Search for the product

 @positive
  Scenario Outline: User search for the valid and available products
    When I try to search for the "<Product>" using the endpoint
    Then The response should return a <ResponseCode> status code
    And The response should contain results for the "<Product>"
    And The response should match with the specification defined in "<SchemaPath>"
    Examples:
      | Product | ResponseCode | SchemaPath                  |
      | apple   | 200          | schema/search_product.json  |
      | mango   | 200          | schema/search_product.json  |
      | tofu    | 200          | schema/search_product.json  |
      | water   | 200          | schema/search_product.json  |

  @negative
  Scenario Outline: User search fot the invalid product
    When I try to search for the "<Product>" using the endpoint
    Then The response should return a <ResponseCode> status code
    And The response should match with the specification defined in "<SchemaPath>"
    Examples:
    | Product | ResponseCode | SchemaPath                       |
    | Tea     | 404          | schema/ProductNotFound.json      |
    |         | 401          | schema/Product_Unauthorised.json |