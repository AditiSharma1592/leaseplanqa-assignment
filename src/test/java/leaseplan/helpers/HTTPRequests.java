package leaseplan.helpers;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public class HTTPRequests {
    public static final String baseURL = "https://waarkoop-server.herokuapp.com/";
    public static final String path = "api/v1/search/test/";

    public static Response GET(String resource) {
        return SerenityRest.given().spec(HTTPRequests.getBasicRequestSpec())
                .get(resource).then().extract().response();
    }

    private static RequestSpecification getBasicRequestSpec() {
        return new RequestSpecBuilder().setBaseUri(baseURL)
                .setBasePath(path)
                .setContentType(ContentType.JSON)
                .build();
    }
}
