package leaseplan.actions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import leaseplan.helpers.HTTPRequests;
import net.serenitybdd.rest.SerenityRest;
import java.util.HashMap;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;


import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.then;


public class SearchProductsActions {

    public Response searchTestProducts(String product) {
    	 return HTTPRequests.GET(product);
    }

    public void verifyProductIsInResponseResult(String ExpectedProduct){
    	 List<HashMap<String, Object>> Results = lastResponse().jsonPath().getList("$");
         System.out.println(Results);
         assertThat(Results).anyMatch(product -> product.get("title").toString().contains(ExpectedProduct));      
    }
    
    public void verifyResponseCode(int responseCode) {
    	 then().statusCode(responseCode);
    }

    public void verifySchema(String schemaPath) {
    	then().body(matchesJsonSchemaInClasspath(schemaPath));
    }
}
