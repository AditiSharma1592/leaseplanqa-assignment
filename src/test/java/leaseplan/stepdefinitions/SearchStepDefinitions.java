package leaseplan.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import leaseplan.actions.SearchProductsActions;
import net.thucydides.core.annotations.Steps;

public class SearchStepDefinitions {
	

    @Steps
    public SearchProductsActions searchProductActions;

	@When("I try to search for the {string} using the endpoint")
	public void i_try_to_search_for_the_apple_using_the_endpoint(String InputProduct) {
		 searchProductActions.searchTestProducts(InputProduct);
	}
	
	@Then("The response should return a {int} status code")
	public void the_response_should_return_a_status_code(int ActualResponseCode) {
		 searchProductActions.verifyResponseCode(ActualResponseCode);
	}
	
	@Then("The response should contain results for the {string}")
	public void the_response_should_contain_results_for_the(String ExpectedProduct) {
		searchProductActions.verifyProductIsInResponseResult(ExpectedProduct);
	}
	
	@Then("The response should match with the specification defined in {string}")
	public void the_response_should_match_with_the_specification_defined_in(String SchemaPath) {
		searchProductActions.verifySchema(SchemaPath);
	}


}
